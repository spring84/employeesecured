package com.visiontotale.EmployeeSecured.service.impl;

import com.visiontotale.EmployeeSecured.exception.AppRoleNotFoundException;
import com.visiontotale.EmployeeSecured.exception.AppUserNotFoundException;
import com.visiontotale.EmployeeSecured.mapper.AppUserMapper;
import com.visiontotale.EmployeeSecured.model.dto.AppUserDTO;
import com.visiontotale.EmployeeSecured.model.entities.AppRoleEntity;
import com.visiontotale.EmployeeSecured.model.entities.AppUserEntity;
import com.visiontotale.EmployeeSecured.repository.AppRoleRepository;
import com.visiontotale.EmployeeSecured.repository.AppUserRepository;
import com.visiontotale.EmployeeSecured.service.AccountService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.UUID;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AppUserRepository appUserRepository;
    private final AppRoleRepository appRoleRepository;
    private final PasswordEncoder passwordEncoder;
    private final AppUserMapper appUserMapper;

    @Override
    public AppUserEntity addNewUser(AppUserEntity appUserEntity) {
        log.info("Saving a new User");
        String password = UUID.randomUUID().toString();
        String reducedPassword = password.substring(0, Math.min(password.length(), 8));
        appUserEntity.setPassword(passwordEncoder.encode(reducedPassword));
        appUserEntity.setCode(UUID.randomUUID().toString());

        AppUserEntity savedUser = appUserRepository.save(appUserEntity);
        savedUser.setPassRet(reducedPassword);

        return savedUser;
    }

    @Override
    public AppRoleEntity addNewRole(AppRoleEntity appRoleEntity) {
        log.info("Saving a new Role");
        return appRoleRepository.save(appRoleEntity);
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        log.info("Adding role to a User");
        AppUserEntity appUserEntity = appUserRepository.findByUserName(userName);
        if (appUserEntity == null) {
            throw new AppUserNotFoundException("The User with userName [" + userName + "] could not be found");
        }

        AppRoleEntity appRoleEntity = appRoleRepository.findByRoleName(roleName);
        if (appRoleEntity == null) {
            throw new AppRoleNotFoundException("The Role with roleName [" + roleName + "] could not be found");
        }

        appUserEntity.getAppRoleEntities().add(appRoleEntity);
    }

    @Override
    public AppUserEntity loadUserByUserName(String userName) {
        log.info("Loading User : " + userName);
        return appUserRepository.findByUserName(userName);
    }

    @Override
    public List<AppUserEntity> listUsers() {
        log.info("Listing all Users");
        return appUserRepository.findAll();
    }

    @Override
    public List<AppRoleEntity> listRoles() {
        log.info("Listing all Roles");
        return appRoleRepository.findAll();
    }

    @Override
    public AppUserEntity addNewUserWithRoles(AppUserDTO appUserDTO) {
        if (CollectionUtils.isEmpty(appUserDTO.getRoleNames())) {
            throw new AppRoleNotFoundException("No roles sets when adding a new User with roles");
        }
        return addNewUser(appUserMapper.dtoToEntity(appUserDTO));
    }

    @Override
    public boolean checkEmail(String email) {
        return ObjectUtils.isNotEmpty(appUserRepository.findByEmail(email));
    }
}

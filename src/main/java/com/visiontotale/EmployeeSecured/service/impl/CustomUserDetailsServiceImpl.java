package com.visiontotale.EmployeeSecured.service.impl;

import com.visiontotale.EmployeeSecured.model.entities.AppUserEntity;
import com.visiontotale.EmployeeSecured.service.AccountService;
import lombok.AllArgsConstructor;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Service
@AllArgsConstructor
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private final AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUserEntity appUserEntity = accountService.loadUserByUserName(username);
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        appUserEntity.getAppRoleEntities().forEach(
                appRoleEntity -> authorities.add(new SimpleGrantedAuthority(appRoleEntity.getRoleName())));

        return new User(appUserEntity.getUserName(), appUserEntity.getPassword(), authorities);
    }
}

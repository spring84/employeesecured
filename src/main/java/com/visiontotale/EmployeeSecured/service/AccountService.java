package com.visiontotale.EmployeeSecured.service;

import com.visiontotale.EmployeeSecured.model.dto.AppUserDTO;
import com.visiontotale.EmployeeSecured.model.entities.AppRoleEntity;
import com.visiontotale.EmployeeSecured.model.entities.AppUserEntity;

import java.util.List;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

public interface AccountService {
    AppUserEntity addNewUser(AppUserEntity appUserEntity);
    AppRoleEntity addNewRole(AppRoleEntity appRoleEntity);
    void addRoleToUser(String userName, String roleName);
    AppUserEntity loadUserByUserName(String userName);
    List<AppUserEntity> listUsers();
    List<AppRoleEntity> listRoles();
    AppUserEntity addNewUserWithRoles(AppUserDTO appUserDTO);
    boolean checkEmail(String email);
}

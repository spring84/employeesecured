package com.visiontotale.EmployeeSecured.mapper;

import com.visiontotale.EmployeeSecured.model.dto.AppUserDTO;
import com.visiontotale.EmployeeSecured.model.entities.AppUserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Mapper(componentModel = "spring", uses = AppRoleQualifier.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AppUserMapper {
    @Mapping(source = "roleNames", target = "appRoleEntities", qualifiedByName = "roleNameToAppRoleEntity")
    AppUserEntity dtoToEntity(AppUserDTO appUserDTO);

    @Mapping(source = "appRoleEntities", target = "roleNames", qualifiedByName = "appRoleEntityToRoleName")
    AppUserDTO entityToDto(AppUserEntity appUserEntity);
}

package com.visiontotale.EmployeeSecured.mapper;

import com.visiontotale.EmployeeSecured.model.entities.AppRoleEntity;
import com.visiontotale.EmployeeSecured.repository.AppRoleRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Component
@Mapper
public class AppRoleQualifier {

    @Autowired
    private AppRoleRepository appRoleRepository;

    @Named("roleNameToAppRoleEntity")
    List<AppRoleEntity> roleNameToAppRoleEntity(List<String> roleNames) {
        return roleNames.stream()
                .map(roleName -> roleName == null ? null : appRoleRepository.findByRoleName(roleName))
                .collect(Collectors.toList());
    }

    @Named("appRoleEntityToRoleName")
    List<String> appRoleEntityToRoleName(List<AppRoleEntity> appRoleEntities) {
        return appRoleEntities.stream()
                .map(appRoleEntity -> appRoleEntity == null ? null : appRoleEntity.getRoleName())
                .collect(Collectors.toList());
    }
}

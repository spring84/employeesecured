package com.visiontotale.EmployeeSecured.exception;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

public class AppRoleNotFoundException extends BusinessException {
    public AppRoleNotFoundException(String message) {
        super(message);
    }
}

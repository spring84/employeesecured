package com.visiontotale.EmployeeSecured.exception;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

public class AppUserNotFoundException extends BusinessException {
    public AppUserNotFoundException(String message) {
        super(message);
    }
}

package com.visiontotale.EmployeeSecured.exception;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

public class BusinessException extends RuntimeException {
    public BusinessException(String message) {
        super(message);
    }
}

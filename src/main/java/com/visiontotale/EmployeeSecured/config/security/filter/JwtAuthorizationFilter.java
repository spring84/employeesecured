package com.visiontotale.EmployeeSecured.config.security.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.JWTVerifier;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.visiontotale.EmployeeSecured.utility.JWTHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Slf4j
public class JwtAuthorizationFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        log.info("Checking Authorization");
        if (request.getServletPath().equals("/refreshToken") /*||  request.getServletPath().equals("/swagger-ui")*/) {
            filterChain.doFilter(request, response);
        } else {
            String authorizationToken = request.getHeader(JWTHelper.AUTH_HEADER);
            if (authorizationToken != null && authorizationToken.startsWith(JWTHelper.PREFIX)) {
                try {
                    String jwtToken = authorizationToken.substring(JWTHelper.PREFIX.length());
                    final Algorithm algorithm = Algorithm.HMAC256(JWTHelper.SECRET);
                    final JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                    final DecodedJWT decodedJWT = jwtVerifier.verify(jwtToken.replaceAll("\\s+",""));
                    String userName = decodedJWT.getSubject();
                    String[] roles = decodedJWT.getClaim("roles").asArray(String.class);

                    Collection<GrantedAuthority> authorities = new ArrayList<>();
                    for (String role : roles) {
                        authorities.add(new SimpleGrantedAuthority(role));
                    }
                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userName, null, authorities);
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);

                    filterChain.doFilter(request, response);
                } catch (Exception e) {
                    response.setHeader("errorMessage", e.getMessage());
                    response.sendError(HttpServletResponse.SC_FORBIDDEN);
                }
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }
}

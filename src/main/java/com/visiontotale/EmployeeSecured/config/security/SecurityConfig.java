package com.visiontotale.EmployeeSecured.config.security;

import com.visiontotale.EmployeeSecured.config.security.filter.JwtAuthenticationFilter;
import com.visiontotale.EmployeeSecured.config.security.filter.JwtAuthorizationFilter;
import com.visiontotale.EmployeeSecured.service.impl.CustomUserDetailsServiceImpl;

import lombok.AllArgsConstructor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.http.HttpMethod;

import java.util.Arrays;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final CustomUserDetailsServiceImpl userDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        // No session will be created or used by Spring Security
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.headers().frameOptions().disable();

        http.authorizeRequests()
                .antMatchers(
                        HttpMethod.POST,
                        "/users/**",
                        "/roles/**",
                        "/addRoleToUser/**",
                        "/users-roles/**",
                        "/check-email/**")
                .hasAuthority("ADMIN");

        http.authorizeRequests()
                .antMatchers(
                        HttpMethod.GET,
                        "/users/**")
                .hasAuthority("USER");

        http.authorizeRequests()
                .antMatchers(
                        "/h2-console/**",
                        "/refreshToken/**",
                        "/login/**",
                        "/swagger-ui/**",
                        "/v3/api-docs/**").permitAll();

        http.authorizeRequests()
                .anyRequest()
                .authenticated();

        http.logout()
                .logoutUrl("/logout")
                .logoutUrl("/")
                .invalidateHttpSession(true);

        http.cors();
        http.addFilter(new JwtAuthenticationFilter(authenticationManagerBean()));
        http.addFilterBefore(new JwtAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
        configuration.setExposedHeaders(Arrays.asList("x-auth-token"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }
}

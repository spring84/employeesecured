package com.visiontotale.EmployeeSecured.config;

import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author Arnaud
 * 01/11/2022
 *
 */

@Component
public class SwaggerConfig {
    String[] paths = { "/**" };
    String[] packages = {"com.visiontotale.EmployeeSecured.api"};

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("Employee")
                .pathsToMatch(paths)
                .packagesToScan(packages)
                .addOpenApiCustomiser(openApi -> {
                    openApi.info(new Info()
                            .title("Secured Employee APIs")
                            .description("Exposing Employees Operations")
                            .version("1.0")
                    );
                })
                .build();
    }
}

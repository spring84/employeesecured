package com.visiontotale.EmployeeSecured.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arnaud
 * 01/11/2022
 *
 */

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "app_user")
public class AppUserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Please enter a user name")
    private String userName;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(message = "Please enter a password")
    private String password;
    @Email(message = "Please enter a right email!")
    private String email;
    @NotBlank(message = "Please enter a user job title")
    private String jobTitle;
    @NotBlank(message = "Please enter a user phone number")
    private String phone;
    private String code;

    @Transient
    private String passRet;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<AppRoleEntity> appRoleEntities = new ArrayList<>();
}

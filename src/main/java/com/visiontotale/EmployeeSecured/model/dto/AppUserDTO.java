package com.visiontotale.EmployeeSecured.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppUserDTO {
    @NotBlank(message = "Please enter a user name")
    private String userName;
    @Email(message = "Please enter a right email!")
    private String email;
    private List<String> roleNames;
}

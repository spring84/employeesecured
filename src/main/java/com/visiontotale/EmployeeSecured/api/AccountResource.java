package com.visiontotale.EmployeeSecured.api;

import com.visiontotale.EmployeeSecured.model.dto.AppUserDTO;
import com.visiontotale.EmployeeSecured.model.entities.AppUserEntity;
import com.visiontotale.EmployeeSecured.service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

import java.util.List;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Slf4j
@RestController
@RequestMapping("/users")
@Tag(name = "Account Ressource")
@AllArgsConstructor
public class AccountResource {

    private final AccountService accountService;

    @GetMapping("/all")
    @Operation(summary = "Get all the employees")
    public ResponseEntity<List<AppUserEntity>> getAllAppUsers() {
        return new ResponseEntity<> (accountService.listUsers(), HttpStatus.OK);
    }

    @PostMapping("/add")
    @Operation(summary = "Add an employee")
    public ResponseEntity<AppUserEntity> addEmployee(@RequestBody @Valid AppUserEntity appUserEntity) {
        return new ResponseEntity<>(
                accountService.addNewUser(appUserEntity),
                HttpStatus.CREATED);
    }

    @PostMapping("/users-roles")
    @Operation(summary = "Add an employee with roles")
    public ResponseEntity<AppUserEntity> addEmployeeWithRoles(@RequestBody @Valid AppUserDTO appUserDTO) {
        return new ResponseEntity<>(
                accountService.addNewUserWithRoles(appUserDTO),
                HttpStatus.OK);
    }

}

package com.visiontotale.EmployeeSecured.utility;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

public class JWTHelper {
    private JWTHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static final String SECRET = "SecretForJwtToken1569";
    public static final String AUTH_HEADER = "Authorization";
    public static final String PREFIX = "Bearer ";
    public static final long EXPIRE_ACCESS_TOKEN = 10*60*1000;
    public static final long EXPIRE_REFRESH_TOKEN = 60*60*1000;
}

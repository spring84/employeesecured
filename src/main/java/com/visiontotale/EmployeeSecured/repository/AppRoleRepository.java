package com.visiontotale.EmployeeSecured.repository;

import com.visiontotale.EmployeeSecured.model.entities.AppRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Repository
public interface AppRoleRepository extends JpaRepository<AppRoleEntity, Long> {
    AppRoleEntity findByRoleName(String roleName);
}

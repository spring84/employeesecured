package com.visiontotale.EmployeeSecured.repository;

import com.visiontotale.EmployeeSecured.model.entities.AppUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Arnaud
 * 02/11/2022
 *
 */

@Repository
public interface AppUserRepository extends JpaRepository<AppUserEntity, Long> {
    AppUserEntity findByUserName(String userName);
    AppUserEntity findByEmail(String email);
}
